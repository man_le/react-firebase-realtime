import React, { Component } from 'react';
import ReactTable from "react-table";
import _ from 'lodash';
import 'react-table/react-table.css';
import './page.css';
import firebase from 'firebase';
import { DB_CONFIG } from '../../config';

class Landing extends Component {
  constructor() {
    super();

    this.app = firebase.initializeApp(DB_CONFIG);
    this.database = this.app.database().ref().child('dataset');

    this.onChange = _.debounce(this.onChange.bind(this),500);

    this.state = {
      data: []
    }

    this.database.on('value', snap => {
      this.setState({ data: snap.val() })
    })
  }

  onChange(key, value, index){
    this.database.child(index).update({[key]: value});
  }

  render() {
    const { data } = this.state;
    const columns = [
      {
        Header: 'Tag',
        accessor: 'tag',
        Cell: props => <input autoComplete="none" className='rd-text' type='text' defaultValue={props.value} onChange={(e)=>this.onChange('tag',e.target.value, props.index)}  />
      },
      {
        Header: 'First Name',
        accessor: 'firstName',
        Cell: props => <input autoComplete="none" className='rd-text' type='text' defaultValue={props.value} onChange={(e)=>this.onChange('firstName',e.target.value, props.index)} />
      }, {
        Header: 'Last Name',
        accessor: 'lastName',
        Cell: props => <input autoComplete="none" className='rd-text' type='text' defaultValue={props.value} onChange={(e)=>this.onChange('lastName',e.target.value, props.index)} />
      }, {
        Header: 'Email',
        accessor: 'email',
        Cell: props => <input autoComplete="none" className='rd-text' type='text' defaultValue={props.value} onChange={(e)=>this.onChange('email',e.target.value, props.index)} />
      }, {
        Header: 'Address',
        accessor: 'address',
        Cell: props => <input autoComplete="none" className='rd-text' type='text' defaultValue={props.value} onChange={(e)=>this.onChange('address',e.target.value, props.index)} />
      }, {
        Header: 'Age',
        accessor: 'age',
        Cell: props => <input autoComplete="none" className='rd-text' type='text' defaultValue={props.value} onChange={(e)=>this.onChange('age',e.target.value, props.index)} />
      }]
    return <ReactTable
      resizable={true}
      showPagination={false}
      showPaginationBottom={false}
      data={data}
      columns={columns}
      defaultPageSize={240}
    />

  }
}

export default Landing;
